﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SisDog.Models
{
    public class Adotante
    {
        public int perfilId { get; set; } //chave principal
        public string nome { get; set; }
        public string datanascimento { get; set; }
        public string cpf { get; set; }
        public string rg { get; set; }
        public string genero { get; set; }
        public string telefoneFixo { get; set; }
        public string telefoneCelular { get; set; }
        public string email { get; set; }
        public string rua { get; set; }
        public int numero { get; set; }
        public string bairro { get; set; }
        public string cidade { get; set; }
        public string estado { get; set; }
        public int qtdAnimais { get; set; }
        public string adotante { get; set; }

        public Usuario Usuario { get; set; }
        public int usuarioId { get; set; }
        public List<Adocao> Adocoes { get; set; }

    }
}
