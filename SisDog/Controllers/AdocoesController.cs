﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SisDog.Data;
using SisDog.Models;

namespace SisDog.Controllers
{
    [Route("api/sisdog/[controller]")]
    [ApiController]
    public class AdocoesController : ControllerBase
    {
        private readonly SisDogContext _context;

        public AdocoesController(SisDogContext context)
        {
            _context = context;
        }

        // GET: api/Adocoes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Adocao>>> GetAdocoes()
        {
            return await _context.Adocoes.ToListAsync();
        }

        // GET: api/Adocoes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Adocao>> GetAdocao(int id)
        {
            var adocao = await _context.Adocoes.FindAsync(id);

            if (adocao == null)
            {
                return NotFound();
            }

            return adocao;
        }

        // PUT: api/Adocoes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAdocao(int id, Adocao adocao)
        {
            if (id != adocao.codigo)
            {
                return BadRequest();
            }

            _context.Entry(adocao).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AdocaoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Adocoes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Adocao>> PostAdocao(Adocao adocao)
        {
            _context.Adocoes.Add(adocao);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (AdocaoExists(adocao.codigo))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetAdocao", new { id = adocao.codigo }, adocao);
        }

        // DELETE: api/Adocoes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Adocao>> DeleteAdocao(int id)
        {
            var adocao = await _context.Adocoes.FindAsync(id);
            if (adocao == null)
            {
                return NotFound();
            }

            _context.Adocoes.Remove(adocao);
            await _context.SaveChangesAsync();

            return adocao;
        }

        private bool AdocaoExists(int id)
        {
            return _context.Adocoes.Any(e => e.codigo == id);
        }
    }
}
