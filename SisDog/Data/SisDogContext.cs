﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SisDog.Models;

namespace SisDog.Data
{
    public class SisDogContext : DbContext
    {
        public SisDogContext(DbContextOptions<SisDogContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Adocao>(entity =>
            {
                entity.HasKey(e => new { e.codigo });
            });

            modelBuilder.Entity<Adotante>(entity =>
            {
                entity.HasKey(e => new { e.perfilId });
            });

            modelBuilder.Entity<Adocao>()
                    .HasOne(p => p.Adotante)
                    .WithMany(b => b.Adocoes)
                    .HasForeignKey(p => p.perfilId)
                    .HasPrincipalKey(c => c.perfilId);

            modelBuilder.Entity<Adocao>()
                    .HasOne(p => p.Animal)
                    .WithMany(b => b.Adocoes)
                    .HasForeignKey(p => p.animalId)
                    .HasPrincipalKey(c => c.animalId);

            modelBuilder.Entity<Usuario>()
                    .HasOne(b => b.Adotante)
                    .WithOne(i => i.Usuario)
                    .HasForeignKey<Adotante>(b => b.usuarioId);

        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Adotante> Adotantes { get; set; }
        public DbSet<Animal> Animals { get; set; }
        public DbSet<Adocao> Adocoes { get; set; }

    }
}
