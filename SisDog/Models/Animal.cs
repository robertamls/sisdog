﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SisDog.Models
{
    public class Animal
    {
        public int animalId { get; set; } // chave principal
        public string nome { get; set; }
        public string raca { get; set; }
        public string idade { get; set; }
        public string genero { get; set; }
        public string deficiencia { get; set; }
        public string castrado { get; set; }
        public string vacinado { get; set; }
        public string tamanho { get; set; }
        public string peso { get; set; }
        public string cor { get; set; }
        public string dataResgate { get; set; }
        public string observacao { get; set; }

        public List<Adocao> Adocoes { get; set; }
    }
}

