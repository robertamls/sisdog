﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace SisDog.Migrations
{
    public partial class migracao : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Animals",
                columns: table => new
                {
                    animalId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    nome = table.Column<string>(nullable: true),
                    raca = table.Column<string>(nullable: true),
                    idade = table.Column<string>(nullable: true),
                    genero = table.Column<string>(nullable: true),
                    deficiencia = table.Column<string>(nullable: true),
                    castrado = table.Column<string>(nullable: true),
                    vacinado = table.Column<string>(nullable: true),
                    tamanho = table.Column<string>(nullable: true),
                    peso = table.Column<string>(nullable: true),
                    cor = table.Column<string>(nullable: true),
                    dataResgate = table.Column<string>(nullable: true),
                    observacao = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Animals", x => x.animalId);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    usuarioId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    login = table.Column<string>(nullable: true),
                    senha = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.usuarioId);
                });

            migrationBuilder.CreateTable(
                name: "Adotantes",
                columns: table => new
                {
                    perfilId = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    nome = table.Column<string>(nullable: true),
                    datanascimento = table.Column<string>(nullable: true),
                    cpf = table.Column<string>(nullable: true),
                    rg = table.Column<string>(nullable: true),
                    genero = table.Column<string>(nullable: true),
                    telefoneFixo = table.Column<string>(nullable: true),
                    telefoneCelular = table.Column<string>(nullable: true),
                    email = table.Column<string>(nullable: true),
                    rua = table.Column<string>(nullable: true),
                    numero = table.Column<int>(nullable: false),
                    bairro = table.Column<string>(nullable: true),
                    cidade = table.Column<string>(nullable: true),
                    estado = table.Column<string>(nullable: true),
                    qtdAnimais = table.Column<int>(nullable: false),
                    adotante = table.Column<string>(nullable: true),
                    usuarioId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adotantes", x => x.perfilId);
                    table.ForeignKey(
                        name: "FK_Adotantes_Usuarios_usuarioId",
                        column: x => x.usuarioId,
                        principalTable: "Usuarios",
                        principalColumn: "usuarioId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Adocoes",
                columns: table => new
                {
                    codigo = table.Column<int>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    dataAdocao = table.Column<string>(nullable: true),
                    devolvido = table.Column<string>(nullable: true),
                    motivo = table.Column<string>(nullable: true),
                    animalId = table.Column<int>(nullable: false),
                    perfilId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Adocoes", x => x.codigo);
                    table.ForeignKey(
                        name: "FK_Adocoes_Animals_animalId",
                        column: x => x.animalId,
                        principalTable: "Animals",
                        principalColumn: "animalId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Adocoes_Adotantes_perfilId",
                        column: x => x.perfilId,
                        principalTable: "Adotantes",
                        principalColumn: "perfilId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Adocoes_animalId",
                table: "Adocoes",
                column: "animalId");

            migrationBuilder.CreateIndex(
                name: "IX_Adocoes_perfilId",
                table: "Adocoes",
                column: "perfilId");

            migrationBuilder.CreateIndex(
                name: "IX_Adotantes_usuarioId",
                table: "Adotantes",
                column: "usuarioId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Adocoes");

            migrationBuilder.DropTable(
                name: "Animals");

            migrationBuilder.DropTable(
                name: "Adotantes");

            migrationBuilder.DropTable(
                name: "Usuarios");
        }
    }
}
