﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SisDog.Models
{
    public class Adocao
    {
        public int codigo { get; set; }
        public string dataAdocao { get; set; }
        public string devolvido { get; set; }
        public string motivo { get; set; }

        public Animal Animal { get; set; }
        public int animalId { get; set; } // chave estrangeira
        public Adotante Adotante { get; set; }
        public int perfilId { get; set; } // chave estrangeira
    }
}
