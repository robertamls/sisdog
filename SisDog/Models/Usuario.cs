﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SisDog.Models
{
    public class Usuario
    {
        public int usuarioId { get; set; }
        public string login { get; set; }
        public string senha { get; set; }

       public Adotante Adotante { get; set; }
    }
}