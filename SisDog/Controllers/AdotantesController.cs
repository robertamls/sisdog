﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SisDog.Data;
using SisDog.Models;

namespace SisDog.Controllers
{
    [Route("api/sisdog/[controller]")]
    [ApiController]
    public class AdotantesController : ControllerBase
    {
        private readonly SisDogContext _context;

        public AdotantesController(SisDogContext context)
        {
            _context = context;
        }

        // GET: api/Adotantes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Adotante>>> GetAdotantes()
        {
            return await _context.Adotantes.ToListAsync();
        }

        // GET: api/Adotantes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Adotante>> GetAdotante(int id)
        {
            var adotante = await _context.Adotantes.FindAsync(id);

            if (adotante == null)
            {
                return NotFound();
            }

            return adotante;
        }

        // PUT: api/Adotantes/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutAdotante(int id, Adotante adotante)
        {
            if (id != adotante.perfilId)
            {
                return BadRequest();
            }

            _context.Entry(adotante).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!AdotanteExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Adotantes
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Adotante>> PostAdotante(Adotante adotante)
        {
            _context.Adotantes.Add(adotante);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAdotante", new { id = adotante.perfilId }, adotante);
        }

        // DELETE: api/Adotantes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Adotante>> DeleteAdotante(int id)
        {
            var adotante = await _context.Adotantes.FindAsync(id);
            if (adotante == null)
            {
                return NotFound();
            }

            _context.Adotantes.Remove(adotante);
            await _context.SaveChangesAsync();

            return adotante;
        }

        private bool AdotanteExists(int id)
        {
            return _context.Adotantes.Any(e => e.perfilId == id);
        }
    }
}
